package com.hypothesis;

import java.util.HashMap;
import java.util.Map;

public class Utils {

	public static Map<Integer, Integer> getDaysInMonths() {
		Map<Integer, Integer> mapMonthDays = new HashMap<>();

		mapMonthDays.put(1, 31);
		mapMonthDays.put(2, 28);
		mapMonthDays.put(3, 31);
		mapMonthDays.put(4, 30);
		mapMonthDays.put(5, 31);
		mapMonthDays.put(6, 30);
		mapMonthDays.put(7, 31);
		mapMonthDays.put(8, 31);
		mapMonthDays.put(9, 30);
		mapMonthDays.put(10, 31);
		mapMonthDays.put(11, 30);
		mapMonthDays.put(12, 31);

		return mapMonthDays;
	}

	
	private static boolean isLeapYear(int year) {

		if ((year % 4 == 0) && (year % 100 != 0))
			return true;
		if (year % 400 == 0)
			return true;

		return false;
	}

	private static int dateToDay(CustomDate date){
		int days = 0;
		Map<Integer, Integer> mapMonthDays = getDaysInMonths();
		for(int i = 1; i < date.getMonth(); i++){
			if((i == 2) && isLeapYear(date.getYear()))
				days += mapMonthDays.get(i) + 1;
			else
				days += mapMonthDays.get(i);
		}
		
		return days + date.getDay();
	}

	public static int days(CustomDate date1, CustomDate date2) {
		CustomDate startDate = date1, endDate = date2;
		if(date1.getYear() > date2.getYear()){
			startDate = date2;
			endDate = date1;
		}else if(date1.getYear() == date2.getYear()){
			if(date1.getMonth() > date2.getMonth()){
				startDate = date2;
				endDate = date1;
			}else if(date1.getMonth() == date2.getMonth()){
				if(date1.getDay() > date2.getDay()){
					startDate = date2;
					endDate = date1;
				}
			}
		}
		
//		System.out.println("startDate: " + startDate.getDay() + "-" + startDate.getMonth()+ "-" + startDate.getYear()+ " : " + dateToDay(startDate));
//		System.out.println("endDate: " + endDate.getDay() + "-" + endDate.getMonth()+ "-" + endDate.getYear()+ " : " + dateToDay(endDate));

		int day = 0;
		
		if(endDate.getYear() > startDate.getYear()){
			if(isLeapYear(startDate.getYear())){
				day = 366 - dateToDay(startDate);
			}else{
				day = 365 - dateToDay(startDate);
			}
			
			for(int i = startDate.getYear() + 1; i < endDate.getYear(); i++){
				if(isLeapYear(i)){
					day+=366;
				}else{
					day+=365;
				}
			}
			
			day += dateToDay(endDate) - 1;
		}else{
			day = dateToDay(endDate) - dateToDay(startDate) - 1;
		}
				
		
		return day;
	}

}
