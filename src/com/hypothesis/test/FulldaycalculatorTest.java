package com.hypothesis.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.hypothesis.CustomDate;
import com.hypothesis.Utils;

public class FulldaycalculatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testFullDays() {
		
		// 1. 02/06/1983 - 22/06/1983: 19 days
		// 2. 04/07/1984 - 25/12/1984: 173 days
		// 3. 03/01/1989 - 03/08/1983: 1979 days
		
		CustomDate cDate1;
		CustomDate cDate2;
		
		final String tc1date1 = "02/06/1983";
		final String tc1date2 = "22/06/1983";
		final String tc2date1 = "04/07/1984";
		final String tc2date2 = "25/12/1984";
		final String tc3date1 = "03/01/1989";
		final String tc3date2 = "03/08/1983";
		
		cDate1 = new CustomDate(tc1date1);
		cDate2 = new CustomDate(tc1date2);
		
		assertEquals(Utils.days(cDate1, cDate2), 19);
		
		cDate1 = new CustomDate(tc2date1);
		cDate2 = new CustomDate(tc2date2);
		
		assertEquals(Utils.days(cDate1, cDate2), 173);
		
		cDate1 = new CustomDate(tc3date1);
		cDate2 = new CustomDate(tc3date2);
		
		assertEquals(Utils.days(cDate1, cDate2), 1979);
	}

}
