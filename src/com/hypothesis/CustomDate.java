package com.hypothesis;

public class CustomDate {
	int day;
	int month;
	int year;
	
	public CustomDate(String date){
		String[] s = date.split("/");
		day = Integer.parseInt(s[0]);
		month = Integer.parseInt(s[1]);
		year = Integer.parseInt(s[2]);
	}
	
	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

}
